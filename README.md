# Recipie App proxy

Nginx proxy app fr out recipe app API

### Usage

#### Environment variables

- `LISTEN_PORT` - Port to listen on (default: `8000`)
- `APP_HOST` - Hostname of the App to forward requests to (default: `app`)
- `APP_PORT` - Port of the App to forward requests to (default `9000`)
